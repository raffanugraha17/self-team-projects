<footer class="c-footer">
    <div>GT Apps© {{ date('Y') }} || Developed by <strong><a target="_blank" href="https://google.com">GT Apps</a></strong></div>

    <div class="mfs-auto d-md-down-none">Version <strong class="text-danger">1.0</strong></div>
</footer>
